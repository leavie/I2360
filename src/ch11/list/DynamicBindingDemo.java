package ch11.list;

/**
 *
 * @author leavie
 */
public class DynamicBindingDemo {

    public static void main(String[] args) {
        // add your code here...
        m(new GraduateStudent());
        m(new Student());
        m(new Person());
        m(new Object());
    }

    public static void m(Object x) {
        System.out.println(x.toString());
    }
}

class GraduateStudent extends Student {

}

class Student extends Person {

    @Override
    public String toString() {
        return "Person";
    }

}

class Person extends Object {

    @Override
    public String toString() {
        return "Person";
    }

}
