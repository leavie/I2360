package ch11.keyPoint;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leavie
 */
public class keyPoint11_23 {
        public static void main(String[] args) {
        // add your code here...
        A a = new A();
        B b = new B();
    }
}

class A  {
    int i = 7;
    public A() {
        this.setI(20);
        System.out.println("i from A is " + i);
    }
    
    public void setI(int i) {
        this.i = 2 * i;
    }
}
class B extends A{
    public B() {
        System.out.println("i from B is " + i);
    }
    
    @Override
    public void setI(int i) {
        this.i = 3 * i;
    }
}