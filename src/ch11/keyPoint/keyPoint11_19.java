package ch11.keyPoint;


public class keyPoint11_19 {

    public static void main(String[] args) {
        // add your code here...
        // can you assign new int[50], new Integer[50], new String[50], new Object[50]
        // into a variable fo Object[] type
        
        // yes
        Object[] oaryIntegers = new Integer[50];
        Object[] oaryStrings = new String[50];
        Object[] oaryObjects = new Object[50];
        
        // specail
        Object oIntegers = new Integer[50];
        Object oInts = new int[50];
        
        // No
        // Object[] ?? = new int[50];  
    }
}
