package ch11.keyPoint;

public class keyPoint11_21A {

    public static void main(String[] args) {
        // add your code here...
        // what is outpt
        new Person().printPerson();
        new Student().printPerson();
    }
}

class Student extends Person {

    @Override
    public String getInfo() {
        return "Student";
    }
    
}

class Person {
    public String getInfo() {
        return "Person";
    }
    
    public void printPerson() {
        System.out.println(getInfo());
    }
}
