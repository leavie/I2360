package ch11.keyPoint.adv;




public class keyPoint11_21B {

    public static void main(String[] args) {
        // add your code here...
        // what is outpt
        new Person().printPerson();
       Student stu =  new Student();
               stu.printPerson();
    }
}

class Student extends Person {
    private String getInfo() {
        return "Student";
    }
}

class Person {
    private String getInfo() {
        return "Person";
    }
    
    public void printPerson() {
        System.out.println(getInfo());
    }
}
