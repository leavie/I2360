package ch11.keyPoint;


public class keyPoint11_20 {

    public static void main(String[] args) {
        // add your code here...
        // what is wrong
        
        Integer[] list1 = {1,2,3};
        Double[] list2 = {1.2,2.0,3.1};
        
        int[] list3 = {1,2,3};
        Object[] list4 = {1,2,3};
                        
        printArray(list1);
        printArray(list2);
//        printArray(list3); // int[] 不能轉成 Object[]
    }
    
    public static void printArray(Object[] list) {
        for(Object o:list)
            System.out.print(o + " ");
        System.out.println();
    }
}
