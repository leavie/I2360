/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.robot;

import java.util.ArrayList;
import java.util.Objects;
import uva.shared.Position;

/**
 *
 * @author leavie
 */
public class Robot {

    private static ArrayList<Robot> scentLists = new ArrayList<>(); // 隊友氣味標記清單
    private Position pos; // includes: x-coor,y-coor, direction
    private boolean isLost = false; // 是否走失，掉出了邊界設定為走失狀態

    public Robot(int x, int y, Position.Direction dir) {
        this.pos = new Position(x, y, dir);
    }

    public int getxCoord() {
        return pos.getxCoord();
    }

    public int getyCoord() {
        return pos.getyCoord();
    }

    public void forward() {
        if (!hasScent()) { // 當前所在地點沒有氣味標記，機器人forward()
            pos.forward();
        }
    }

    private void rollback() { // 倒退
        if (!hasScent()) {
            pos.rollback();
        }
    }

    public void turnLeft() { // anti-clockwisely
        pos.turnLeft();

    }

    public void turnRight() { // clockwisely
        pos.turnRight();
    }

    public void tagScent() { // 在地圖中留下標記
        rollback(); // 回到掉落之前的位置狀態
        isLost = true; // 標記為走失
        scentLists.add(this); // 留下氣味標記
    }

    private boolean hasScent() { // 判斷當前的位置狀態是否在標記清單中
        return scentLists.contains(this);
    }

    @Override
    public boolean equals(Object obj) { // 藉由Robot物件的位置的狀態，判斷是否相等
        Robot anoR = null;
        if (obj instanceof Robot) {
            anoR = (Robot) obj; // excliptly casting
        }

        if (anoR == null) { // casting fail
            return false;
        }
        // 位置狀態由三個屬性決定
        return pos.getxCoord() == anoR.pos.getxCoord() && pos.getyCoord() == anoR.pos.getyCoord() && pos.getFaceOn() == anoR.pos.getFaceOn();
    }

    @Override
    public int hashCode() { // 搭配Object.equals
        int hash = 5;
        hash = 67 * hash + this.pos.getxCoord();
        hash = 67 * hash + this.pos.getyCoord();
        hash = 67 * hash + Objects.hashCode(this.pos.getFaceOn());
        return hash;
    }

    @Override
    public String toString() {
        return pos.getxCoord() + " " + pos.getyCoord() + " " + pos.getFaceOn()
                + (isLost ? " LOST" : "");
    }

    public static void main(String[] args) {
        // add your code here...
        Robot r1 = new Robot(1, 1, Position.Direction.E);
        Map robotWorld = new Map(5, 3);

        System.out.println("r1 now: " + r1);
        r1.forward();
        r1.turnLeft();
        r1.turnLeft();
        r1.forward();
        System.out.println("r1 now: " + r1);

        System.out.println("Is r1 outOfBound? " + robotWorld.isOutOfEdge(r1));
        System.out.println("Does r1 have smell?: " + r1.hasScent());
        r1.tagScent();
        System.out.println("Does r1 have smell?: " + r1.hasScent());
        System.out.println("r1 now:" + r1);

    }
}
