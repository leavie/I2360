/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.robot;

import java.util.Scanner;
import uva.shared.InputUtil;
import uva.shared.Position.Direction;

/**
 *
 * @author leavie
 */
public class Game {

    public static void main(String[] args) {
        // add your code here...
        String path = "build/classes/uva/robot/uva118.txt";
        Scanner input = InputUtil.inputFromPath(path);
        Robot robot;
        Map map = new Map(input.nextInt(), input.nextInt()); // 讀取地圖尺寸

        while (input.hasNextInt()) { // 讀取多筆輸入，模擬多個機器人
            robot = new Robot(input.nextInt(), input.nextInt(),
                    Direction.valueOf("" + input.next().charAt(0))); // 讀取位置
            String instruction = input.next(); // 讀取指令

            for (int i = 0; i < instruction.length(); i++) {
                char curIns = instruction.charAt(i); // 擷取當前指令

                // 根據相應指令，控制機器人
                if (curIns == 'F') {
                    robot.forward(); // 前進
                    if (map.isOutOfEdge(robot)) { // 由地圖判斷是否出界
                        robot.tagScent(); // 機器人留下氣味標記
                        break; // 跳出迴圈，出界後的機器人不再執行剩下指令
                    }

                } else if (curIns == 'L') {
                    robot.turnLeft(); // 左轉
                } else {
                    robot.turnRight(); // 右轉
                }
            } // end for 結束執行指令

            System.out.println(robot); // 列印最終狀態
        } // end while
    }
}
