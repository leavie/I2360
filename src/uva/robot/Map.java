/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.robot;

/**
 *
 * @author leavie
 */
public class Map {
    private final int edgeX;
    private final int edgeY;
    
    Map(int x, int y) {
        edgeX = x;
        edgeY = y;
    }
    
    boolean isOutOfEdge(Robot r) {
        if(edgeX < r.getxCoord()) return true;
        if(0 > r.getxCoord()) return true;
        if(edgeY < r.getyCoord()) return true;
        return 0 > r.getyCoord();
    }
}
