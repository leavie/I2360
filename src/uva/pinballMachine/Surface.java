package uva.pinballMachine;

public class Surface {
    private Obstacle[][] map;

    public Surface(int sizeX, int sizeY, int cost) {
        map = new Obstacle[sizeX+1][sizeY+1]; //題目要從座標(1,1)開始
        createBoundary(cost); // 創建並且填充 walls
    }

    public void placeOn(Obstacle obs) {
        int x = obs.getX();
        int y = obs.getY();
        map[x][y] = obs;
    }

    public Obstacle getObstacle(int x, int y) {
        return map[x][y];
    }

    public Obstacle getObstacle(Ball ball) {
        return map[ball.getxCoord()][ball.getyCoord()];
    }

    private void createBoundary(int cost) {
        Wall wall;
        for (int i = 1; i < this.map.length; i++) { //列
            for (int j = 1; j < this.map[1].length; j++) {//行
                if (i == 1 || i == this.map.length - 1) {
                    wall = new Wall(i, j, cost);
                    this.map[i][j] = wall;
                } else if (j == 1 || j == this.map[1].length - 1) {
                    wall = new Wall(i, j, cost);
                    this.map[i][j] = wall;
                }
            }
        }
    }

}
