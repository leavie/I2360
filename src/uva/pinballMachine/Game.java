package uva.pinballMachine;

import java.util.Scanner;
import uva.shared.InputUtil;
import uva.shared.Position;

public class Game {

    public static void main(String[] args) {
        // 宣告物件
        String path = "build/classes/uva/pinballMachine/uva114.txt";

        Surface surf;
        Obstacle bumpr;
        Ball ball;
        Scanner input = InputUtil.inputFromPath(path);
        int totalScores = 0;

        // 讀取輸入
        int edgeX = input.nextInt(); //邊界X
        int edgeY = input.nextInt(); //邊界Y
        int wallCost = input.nextInt(); //撞到牆壁損失或增加多少生命值
        int numberOfBumpers = input.nextInt(); //有多少個bumper

        // 根據輸入建立物件
        // 根據尺寸創建地圖
        surf = new Surface(edgeX, edgeY, wallCost);

        // 根據多少個bumper，創建並且填充 bumpers
        for (int i = 0; i < numberOfBumpers; i++) {
            int x = input.nextInt(); //目前X座標。 1
            int y = input.nextInt(); //目前Y座標。 1
            int point = input.nextInt(); //目前
            int cost = input.nextInt(); //目前

            bumpr = new Bumper(x, y, cost, point);
            // 填充 bumpers, place bumpers on surface
            surf.placeOn(bumpr);
        }

        // 遊戲開始，執行ball
        while (input.hasNextInt()) {
            int x = input.nextInt(); //目前X座標。 1
            int y = input.nextInt(); //目前Y座標。 1

            int direction = input.nextInt(); //目前
            int lifetime = input.nextInt(); //目前
            ball = new Ball(x, y, convertDirection(direction), lifetime);

            while (!ball.isDead()) {
                ball.update(surf);
            }
            totalScores += ball.getScore();
            System.out.println("球的狀態: " + ball);
        }

        System.out.println("總共得幾分: " + totalScores);

    }


    private static Position.Direction convertDirection(int direction_number) {
        switch (direction_number) {  //目前的面向方位
            case 0:  //
                return Position.Direction.E;
            case 1:
                return Position.Direction.N;
            case 2:
                return Position.Direction.W;
            case 3:
                return Position.Direction.S;
        }
        throw new Error("undefined direction"); // 錯誤情況
    }

}
