package uva.pinballMachine;

import uva.shared.Position;
import uva.shared.Position.Direction;

/**
 *
 * @author User
 */
public class Ball {

    // field
    private Position pos;
    private int lifetime;
    private int score;

    // constructor
    public Ball(int x, int y, Direction direction, int lifetime) {
        this.pos = new Position(x, y, direction);
        this.lifetime = lifetime;
        this.score = 0;
    }

    public Ball(Position pos, int lifetime) {
        this.pos = pos;
        this.lifetime = lifetime;
        this.score = 0;
    }

    // public methods
    // getters
    public int getxCoord() {
        return pos.getxCoord();
    }

    public int getyCoord() {
        return pos.getyCoord();
    }

    public Direction getFaceOn() {
        return pos.getFaceOn();
    }

    public int getScore() {
        return score;
    }

    public boolean isDead() {
        return this.lifetime <= 0;
    }

    // override methods
    @Override
    public String toString() {
        return "position: " + pos.toString() + " lifetime: " + this.lifetime + " score: " + this.score;
    }

    // setters
    public void setLife(Obstacle obs) {
        this.lifetime = this.lifetime - obs.getCost();
    }

    public void setScore(Obstacle obs) {
        this.score = this.score + obs.getPoint();
    }

    public void setFaceOn(Obstacle obs) {
        pos.setFaceOn(obs.feedback(this));
    }

    // update
    public void update(Surface surf) { // 週期性更新
        // 偵測障礙物並且發生碰撞
        Obstacle obs = detectCollision(surf); // 在球所在的位置偵測障礙物
        if (obs != null) { // 當有障礙物時
            this.collide(obs); // 與障礙物碰撞
        }
        if (isDead()) {
            return;
        }
        // 向前移動
        pos.forward();
        // 減少一個生命單位
        lifetime--;
    }

    private Obstacle detectCollision(Surface surf) {
        Obstacle obs = surf.getObstacle(this);
        return obs;
    }

    private void collide(Obstacle obs) {
        // 發生碰撞並改變狀態
        setLife(obs);
        if (isDead()) {
            return;
        }
        setScore(obs);
        setFaceOn(obs);
    }
}
