package uva.pinballMachine;

import uva.shared.Position;

public class Wall extends Obstacle {

    public Wall(int x, int y, int cost) {
        super(x, y, cost); // 調用父類別的constructor
    }

    public Wall(Position pos, int cost) {
        super(pos, cost); // 調用父類別的constructor
    }

    @Override
    public Position.Direction feedback(Ball ball) {
        return Position.getBouncedDirection(ball.getFaceOn());
    }
}
