package uva.pinballMachine;

import uva.shared.Position;

abstract public class Obstacle {

    private Position pos;
    private int cost;

    protected Obstacle(int x, int y, int cost) {
        this.pos = new Position(x, y, null);
        this.cost = cost;
    }

    protected Obstacle(Position pos, int cost) {
        this.pos = pos;
        this.cost = cost;
    }


//    public Position getPos() { // 一個getter返回了一個物件的引用違反了封裝性
//        return pos;
//    }

    public int getX() {
        return pos.getxCoord();
    }

    public int getY() {
        return pos.getyCoord();
    }

    public int getCost() {
        return cost;
    }

    public int getPoint() { // Not using abstract instead of hook will be better?
        return 0;
    }

    abstract public Position.Direction feedback(Ball ball);

}
