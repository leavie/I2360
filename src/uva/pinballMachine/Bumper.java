
package uva.pinballMachine;

import uva.shared.Position;

public class Bumper extends Obstacle {

    public int point;

    public Bumper(int x, int y, int cost, int point) { //建構子不能被繼承
        super(x, y, cost); //super指父類別的(Obstacle)
        this.point = point;
    }

    public Bumper(Position pos, int cost, int point) { //建構子不能被繼承
        super(pos, cost); //super指父類別的(Obstacle)
        this.point = point;
    }

    @Override
    public int getPoint() {
        return this.point;
    }

    @Override
    public Position.Direction feedback(Ball ball) {
        return Position.getClockwiseDirection(ball.getFaceOn());
    }
}
