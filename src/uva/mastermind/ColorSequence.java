/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.mastermind;

import java.util.Scanner;

/**
 *
 * @author leavie
 */
public class ColorSequence {
    // Shared class constants
//    static final char[] COLORS = {'C','M','Y','K','R','G'}; // 列舉所有顏色
    static final char[] COLORS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; // 列舉所有顏色
    static final int LENGTH_OF_SEQUENCE = 4; // 序列的長度
    static final int NUMBER_OF_COLORS = COLORS.length; // 幾種顏色

    // Private

    // fields
    private char[] colorSeq = new char[LENGTH_OF_SEQUENCE]; // 顏色序列
    private int pinsOfBlack = 0;
    private int pinsOfWhite = 0;

    // static methods
    private static void clearPins(ColorSequence colorSequence) { // 紀錄歸零
        colorSequence.pinsOfBlack = colorSequence.pinsOfWhite = 0;
    }
    private static void backupColors(ColorSequence one, char[] newColors) {
        System.arraycopy(one.colorSeq, 0, newColors, 0, LENGTH_OF_SEQUENCE);
    }

    // API

    // static methods
    // test driver
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ColorSequence computer = ColorSequence.random();/*電腦出題 */
        ColorSequence player = new ColorSequence(); // 玩家猜測

        System.out.println("computer's riddle: " + computer);/*印出謎底 */
        while (!player.isBingo()) { // 如果不是黑針則重複輸入
            player.setColors(input.next()); // 玩家輸入猜測
            ColorSequence.compare(player, computer); // 比較
            System.out.println("player's guess: " + player);  // 印出玩家猜測提示
        } // 直到全是黑針
        System.out.println("恭喜答對了");
    }

    // utility methods
    /**
     * 產生一個隨機的顏色序列
     *
     * @return ColorSequence
     */
    public static ColorSequence random() {
        String seq = "";
        for (int i = 0; i < LENGTH_OF_SEQUENCE; i++) {
            seq += COLORS[(int) (Math.random() * NUMBER_OF_COLORS)];
        }
        return new ColorSequence(seq);
    }
    /**
     * 比對兩個顏色序列，並由目標紀錄
     * precondition: 目標黑白針歸零，備份目標與來源的顏色序列
     * post condition: 目標與來源的顏色序列還原備份，目標的黑白針狀態改變成比對結果
     * @param target 目標， 接受來源的顏色序列以進行比對，並將比對結果以黑針白針記錄
     * @param src 來源， 提供顏色序列來源以進行比對
     */
    public static void compare(ColorSequence target, ColorSequence src) {
        // 初始化
        char[] newTarget = new char[LENGTH_OF_SEQUENCE];
        char[] newSrc = new char[LENGTH_OF_SEQUENCE];

        // precondition
        backupColors(target, newTarget);
        backupColors(src, newSrc);
        clearPins(target);

        // determine black pins based on both positions and colors are correct
        for (int i = 0; i < LENGTH_OF_SEQUENCE; i++) { // 同時遞增解題者與答題者的位置 i++
            if (newTarget[i] == newSrc[i]) { // 如果兩個序列相同位置的顏色也相同
                newTarget[i] = newSrc[i] = '\0'; // 在兩個序列標記相同的顏色為空（代表已經比較過了）
                target.setPinsOfBlack(target.getPinsOfBlack() + 1); // 黑針數量加一
            }
        }

        // determine white pins based on only colors are correct
        for (int i = 0; i < LENGTH_OF_SEQUENCE; i++) { // 在外迴圈遞增解題者的位置 i++
            if (newTarget[i] == '\0') { // 略過已比較過的顏色
                continue;
            }
            for (int j = 0; j < LENGTH_OF_SEQUENCE; j++) { // 在內回圈遞增答題者的位置 j++
                if (newSrc[j] != '\0') {
                    if (newTarget[i] == newSrc[j]) { // 如果兩個序列的顏色相同
                        newTarget[i] = newSrc[j] = '\0'; // 在兩個序列標記相同的顏色為空（代表已經比較過了）
                        target.setPinsOfWhite(target.getPinsOfWhite() + 1); // 白針數量加一
                        break;
                    }
                }
            }
        }
    }

    // constructors
    /**
     * ColorSequence 的建構子，不接受參數
     */
    public ColorSequence() {
    }
    /**
     * ColorSequence 的建構子，接受一個 String 參數
     * @param input 輸入字串，成為顏色序列的內容
     */
    public ColorSequence(String input) {
        ColorSequence.this.setColors(input);
    }

    // setters
    /**
     * 根據提供的字串設定自己的顏色序列
     *
     * @param input 輸入字串，成為顏色序列的內容
     */
    // TODO: 1. throw exception for
    //          1. 長度
    //          2. 輸入不存在的字符
    //          3. 重複輸入
    public final void setColors(String input) {
        if (input.length() < LENGTH_OF_SEQUENCE) {
            throw new ArithmeticException("長度錯誤");
        }
        input.getChars(0, LENGTH_OF_SEQUENCE, colorSeq, 0);
    }

    // getters
    /**
     * 返回真如果黑針數量為最大值
     *
     * @return boolean
     */
    public boolean isBingo() {
        return getPinsOfBlack() == LENGTH_OF_SEQUENCE;
    }
    /**
     * 確認兩個序列的黑白針狀態是否相同
     *
     * @param otherBlack 黑針數量是過濾標竿
     * @param otherWhite 白針數量是過濾標竿
     * @return 兩個序列黑白針狀態是否相同
     */
    public boolean isPinsMatched(int otherBlack, int otherWhite) { // 返回比對模式是否相等
        return getPinsOfBlack() == otherBlack
                && getPinsOfWhite() == otherWhite;
    }
    /**
     * 生成描述黑白針狀態的字串
     *
     * @return 描述黑白針狀態的字串
     */
    public String pinsToString() { // 返回提示字串
        return "black: " + getPinsOfBlack() + " white: " + getPinsOfWhite();
    }

    public int getPinsOfBlack() {
        return pinsOfBlack;
    }

    public void setPinsOfBlack(int pinsOfBlack) {
        this.pinsOfBlack = pinsOfBlack;
    }

    public int getPinsOfWhite() {
        return pinsOfWhite;
    }

    public void setPinsOfWhite(int pinsOfWhite) {
        this.pinsOfWhite = pinsOfWhite;
    }

    // override methods
    @Override
    public String toString() {
        return "sequence: " +  new String(colorSeq) + ", black: " + getPinsOfBlack() + ", white: " + getPinsOfWhite();
    }


}
