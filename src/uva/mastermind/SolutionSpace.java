/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.mastermind;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author leavie
 */
public class SolutionSpace {
    // shared class constants
    static final char[] COLORS = ColorSequence.COLORS;
    static final int LENGTH_OF_SEQUENCE = ColorSequence.LENGTH_OF_SEQUENCE; // 序列的長度
    static final int NUMBER_OF_COLORS = ColorSequence.NUMBER_OF_COLORS; // 幾種顏色
    // fields
    private ArrayList<ColorSequence> list = new ArrayList<>();
    private ColorSequence guess;
    private int targetBlack; // 過濾的標竿
    private int targetWhite; // 過濾的標竿
    private int times; // 過濾次數

    /* API: public method */

    // test driver
    public static void main(String[] args) { // 使用者出題
        Scanner input = new Scanner(System.in);
        SolutionSpace space = new SolutionSpace();
        ColorSequence guess; // 猜測序列

        while (!space.isBingo()) {
            // 從空間取得（下）一個猜測
            guess = space.next();

            System.out.println("-----------------");
            System.out.println("Turn: " + (space.getTimes()));
            System.out.println("Space left: " + space.list.size());
            System.out.println("My Guess is: " + guess);
            // 提示使用者比對謎底並數一數黑白針的數量，做為過濾標竿
            System.out.println("Compare my guess to your riddle.\nCount on pins for both black and white as clue and input below⬇ ");
            space.setTargets(input.nextInt(), input.nextInt());
            System.out.println("-----------------");
        }
        System.out.println("結束");
    }

    // constructors
    public SolutionSpace() {
        generateAll(LENGTH_OF_SEQUENCE, "");
    }

    // setters
    public void setTargets(int black, int white) {
        targetBlack = black;
        targetWhite = white;
    }

    // getters
    // TODO: 使用delegate方法的話有bug
    public boolean isBingo() {
        // 勝利條件，滿足則結束此函數
        return this.targetBlack == LENGTH_OF_SEQUENCE;
    }

    /**
     * precondition: 一組序列清單，其中存在一個目標
     * post condition: 清單中所有序列皆符合當前過濾條件
     *
     * @return 一個符合過濾條件的顏色序列
     */
    public ColorSequence next() {
        if (guess != null) {
            this.compareAll();
            this.removeNotMatched();
        }

        this.guess = this.pickOut();
        this.times = this.getTimes() + 1;
        return this.guess;
    }

    /* Private method */
    // TODO: space is empty, unable to pick out
    private ColorSequence pickOut() throws Error{ // 從空間中挑出並返回一個序列，可能為空
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new Error("space is empty");
    }

    private void generateAll(int length, String seq) { // 算法：兩個變數的重複排列， 數量為（顏色^長度），遞迴長度，迴圈控制顏色
        if (length == 0) {
            list.add(new ColorSequence(seq));
            return;
        }
        for (int i = 0; i < NUMBER_OF_COLORS; i++) {
            generateAll(length - 1, seq + COLORS[i]);
        }
    }

    private void compareAll() {
        // 目前的猜測序列與所有解題空間裡的剩餘序列互相比對
        list.forEach((seq) -> ColorSequence.compare(seq, guess));
    }

    private void removeNotMatched() { // 在空間中移除所有不符合條件的序列
        list.removeIf(seq -> !seq.isPinsMatched(this.targetBlack, this.targetWhite));
    }

    // alternative
    void remove() {
        int i = 0;
        while(list.size() != i) {
            if (!list.get(i).isPinsMatched(targetBlack, targetWhite)) {
                list.remove(i);
            }
            i++;
        }
    }

    public static void main_testRemoveIf(String[] args) {
        ArrayList<Boolean> list = new ArrayList<>();

        list.add(true);
        list.add(false);
        list.add(false);
        list.add(false);
        list.add(true);
        list.add(false);

        System.out.println(list);
        list.removeIf(e->e);
        System.out.println(list);


    }

    public int size() {
        return list.size();
    }

    public int getTimes() {
        return times;
    }
}
