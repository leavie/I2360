/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.blocks;

import java.util.Scanner;
import uva.shared.InputUtil;

/**
 *
 * @author leavie
 */
public class Game {

    public static void main(String[] args) {
        String path = "build/classes/uva/blocks/uva101.txt";
        Scanner input = InputUtil.inputFromPath(path);
        Arm arm;

        while (input.hasNext()) {
            // 初始化手臂
            arm = new Arm(input.nextInt());
            String instrution;
            while (!(instrution = input.next()).equals("quit")) {
                // 讀取指令
                String op1 = instrution;
                int a = input.nextInt();
                String op2 = input.next();
                int b = input.nextInt();

                if (isValid(a, b, arm)) {
                    OperateArm(op1, op2, arm, a, b);
                }
            }
            arm.printPiles(); // 印出所有pile的結果
        }
    }

    public static boolean isValid(int a, int b, Arm arm) {
        // 判斷指令使否合法
        return a != b && arm.getPileNumber(a) != arm.getPileNumber(b);
    }

    public static void OperateArm(String op1, String op2, Arm arm, int a, int b) {
        // 手臂操作相應指令
        switch (op1) {
            case "move":
                switch (op2) {
                    case "over":
                        arm.moveOver(a, b);
                        break;
                    case "onto":
                        arm.moveOnto(a, b);
                        break;
                }
            case "pile":
                switch (op2) {
                    case "over":
                        arm.pileOver(a, b);
                        break;
                    case "onto":
                        arm.pileOnto(a, b);
                        break;
                }
        }
    }

}
