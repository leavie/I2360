/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.blocks;

/**
 *
 * @author leavie
 */
public class Block {

    private int number; // 自己的編號


    public Block(int n) {
        this.number = n;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return ""+number;
    }
    
    
}
