/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.blocks;

import java.util.ArrayList;

/**
 *
 * @author leavie
 */
public class Pile {

    private ArrayList<Block> al = new ArrayList<>();
    private int number;

    public Pile(int n) {
        al.add(new Block(n));
        number = n;
    }

    public Pile() {
//        System.out.println("空的pile");
    }

    public void push(Block e) {
        al.add(e);
    }

    public Block pop() {
        if (al.isEmpty()) {
            throw new Error(this.number+" "+"空了啊");
        }
        return al.remove(al.size() - 1);
    }

    public Block peek() {
        if (al.isEmpty()) {
            throw new Error(this.number+" "+"空了啊");
        }
        return al.get(al.size() - 1);
    }

    public boolean isEmpty() {
        return al.isEmpty();
    }

    @Override
    public String toString() {
        String result = "";
        int topIndex = al.size() - 1;
        while (topIndex != -1) {
            result = " " + this.al.get(topIndex--) + result;
        }
        return result;
    }
    
    public static void main(String[] args) {
        // add your code here...
        Pile p = new Pile(99);
        p.push(new Block(30));
        p.push(new Block(302));
        p.push(new Block(303));
        System.out.println(p.pop());
        System.out.println(p.peek());
        System.out.println(p);
    }

}
