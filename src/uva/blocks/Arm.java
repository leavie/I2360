/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.blocks;

import java.util.Scanner;

/**
 *
 * @author leavie
 */
public class Arm {

    private Pile[] piles;
    private int[] blockPileRecord; // 紀錄每個block相應的pile編號

    public Arm(int n) {
        this.piles = new Pile[n];
        this.blockPileRecord = new int[n];
        for (int i = 0; i < n; i++) {
            this.piles[i] = new Pile(i);
            this.blockPileRecord[i] = i;
        }
    }

    public void moveOnto(int a, int b) {
        this.resetAbove(b);
        this.moveOver(a, b);
    }

    public void moveOver(int a, int b) {
        this.resetAbove(a);
        transferOneElement(this.findPile(b), this.findPile(a));
        updateBlockPileRecord(a, b);
    }

    public void pileOnto(int a, int b) {
        this.resetAbove(b);
        this.pileOver(a, b);
    }

    public void pileOver(int a, int b) {
        Pile aPile = findPile(a);
        Pile tempPile = new Pile();
        Block currentBlock;
        int currentNumber;

        while (!aPile.isEmpty()) {
            currentBlock = aPile.peek();
            currentNumber = currentBlock.getNumber();
            if (currentNumber != a) {
                transferOneElement(tempPile, aPile);
            } else {
                transferOneElement(tempPile, aPile);
                break;
            }
        }
        while (!tempPile.isEmpty()) {
            transferOneElement(findPile(b), tempPile);
        }
        updateBlockPileRecord(a, b);
    }

    private void updateBlockPileRecord(int dest, int src) { // 更新block與pile對應的紀錄
        this.blockPileRecord[dest] = this.blockPileRecord[src];
    }

    private void transferOneElement(Pile srcPile, Pile destPile) {
        srcPile.push(destPile.pop());
    }

    // 把自己以上的的block都復原到其編號相應的pile上
    private void resetAbove(int targetNumber) {
        Pile targetPile = findPile(targetNumber);
        Block currentBlock;
        int currentNumber;
        while (!targetPile.isEmpty()) {
            currentBlock = targetPile.peek();
            currentNumber = currentBlock.getNumber();
            if (currentNumber == targetNumber) {
                break;
            } else {
                reset(currentNumber, currentBlock, targetPile);
            }
        }
    }

    private void reset(int number, Block block, Pile pile) {
        this.piles[number].push(block);
        this.blockPileRecord[number] = number;
        pile.pop();
    }

    private Pile findPile(int n) { // 返回block所在的pile
        return this.piles[getPileNumber(n)];
    }

    public int getPileNumber(int n) { // 查詢block所在的pile編號
        return this.blockPileRecord[n];
    }

    public void printPiles() { // 由pile堆的低號至高號，每堆pile由下至上
        for (int i = 0; i < this.piles.length; i++) {
            System.out.println(i + " " + this.piles[i].toString());
        }
    }

     public static void main(String[] args) {
         int n = 10;
         Arm testArm = new Arm(n);
         testArm.moveOver(7, 1);
         testArm.moveOver(6, 1);
         testArm.pileOver(8, 6);
         testArm.pileOver(8, 5);
         testArm.moveOver(2, 1);
         testArm.moveOver(4, 9);
         testArm.printPiles();
     }

}
