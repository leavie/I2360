/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.boringPoker;

/**
 *
 * @author leavie
 */
public class Game {

    public static void main(String[] args) {
        // add your code here...
        Deck deck = new Deck();
        Pile piles[] = new Pile[KINDS_OF_CARD]; // 13 spaces for every pile
        boolean isSuccess;
        Card curTopCard;
        Pile startPile;
        Pile nextPile;

        // initialization to statify the precondintion
        isSuccess = false;
        for (int i = 0; i < piles.length; i++) { // create 13 piles representing 1 to K
            piles[i] = new Pile();
        }
        deck.shuffle();
        // deal 52 cards onto 13 piles, every pile has 4 cards
        for (int i = 0; i < CAPACITY_OF_DECK; i++) {// deal 52 cards onto 13 piles, every pile has 4 cards
            piles[i % piles.length].placeOnTop(deck.deal()); // deal and place on 13 piles' top clockwisely
        }

        // print init stats
        System.out.println("---------init----------");
        System.out.println("==============deck==========");
        System.out.println(deck);
        System.out.println("=======piles=====");
        printPiles(piles);

//
        // run simulation till meet end condition...        
        startPile = piles[piles.length - 1]; // start from K pile
        curTopCard = startPile.getTop(); // start pile' top
        while (!curTopCard.isFaceUp()) {
            nextPile = piles[curTopCard.getPoint() - 1];
            curTopCard.expose();
            nextPile.append(curTopCard);
            curTopCard = nextPile.getTop();
        }

        // determine success or not
        // for each
//        for (Card cd : poker.getCards()) {
//            if (!cd.isFaceUp()) {
//                isSuccess = false;
//                break;
//            }
//        }

        // print the stats in the end of simulation        
        System.out.println("----------after run--------");
        System.out.println("===========deck=======");
        System.out.println(deck);

        System.out.println("==========piles========");
        printPiles(piles);
        System.out.println("last card: " + curTopCard);
//        System.out.println("Game success is: " + isSuccess);
    }
    private static int CAPACITY_OF_DECK = Deck.CAPACITY;
    private static int KINDS_OF_CARD = Card.KINDS;

    public static void printPiles(Pile[] piles) {
        for (int i = 0; i < piles.length; i++) { // print every card in 13 piles
            System.out.printf("pile[%02d]:\t%s\n", (i + 1), piles[i]);
        }
    }
}

// Myth
//        Card[] xx= new Card[5];
//        Card[] oo = poker.getCards(); // final
//        oo = xx;
