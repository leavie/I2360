/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.boringPoker;

/**
 *
 * @author leavie
 */
public class Deck {
    public static final int CAPACITY = 52; // 容量
    
    private final Card[] cards = new Card[CAPACITY]; // 52 spaces;
    private int top = 0;

    public Deck() {
        for (int i = 0; i < cards.length; i++) { // 52 times
            cards[i] = new Card((i % Card.KINDS) + 1); // 1...Card.KINDS
        }
    }

    public void print() {
        for (int i = 0; i < cards.length; i++) {
            System.out.printf("%-5s", cards[i]);
            if (i % Card.KINDS == Card.KINDS - 1) {
                System.out.println("");
            }
        }
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < cards.length; i++) {
            result += String.format("%-5s", cards[i]);
            if (i % Card.KINDS == Card.KINDS - 1) {
                result += "\n";
            }
        }
        return result;
    }

    public void shuffle() {
        int randIndex;
        for (int i = 0; i < cards.length; i++) {
            randIndex = (int) (Math.random() * cards.length); // 0...51
            swap(i, randIndex);
        }
    }

    private void swap(int a, int b) {
        Card temp = cards[a];
        cards[a] = cards[b];
        cards[b] = temp;
    }

    public Card deal() {
        if (top > cards.length - 1) {  // outbound
            return null;
        }
        // 0...51
        return cards[top++]; // want a copy object or a ref?
    }
}
