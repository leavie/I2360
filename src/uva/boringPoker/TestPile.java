/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.boringPoker;

/**
 *
 * @author leavie
 */
public class TestPile {
    public static void main(String[] args) {
        // add your code here...
        Pile pile = new Pile();
        pile.append(new Card(1));
        pile.append(new Card(2));
        pile.append(new Card(3));
        pile.append(new Card(4));
        pile.append(new Card(5));

        System.out.println("---append-----");
        pile.print();
        System.out.println("\n---get top-----");
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());

        pile.placeOnTop(new Card(11));
        pile.placeOnTop(new Card(21));
        pile.placeOnTop(new Card(31));
        pile.placeOnTop(new Card(41));
        pile.placeOnTop(new Card(51));

        System.out.println("----------place on top------");
        pile.print();
        System.out.println("\n---get top-----");
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());
        System.out.println(pile.getTop());
        
    }
}
