/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.boringPoker;

/**
 *
 * @author leavie
 */
public class Card {
    public static final int KINDS = 13; // 容量
    
    private int point;
    private boolean faceUp = false;

    Card(int p) {
        point = p;
    }

    public int getPoint() {
        return point;
    }

    public boolean isFaceUp() {
        return faceUp;
    }

    public void expose() {
        this.faceUp = true;
    }

    @Override
    public String toString() {
        String direction = faceUp ? "U" : "D";
        return String.format("%3s", point + direction);
    }

}
