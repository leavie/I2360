/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.boringPoker.non_circulated_array;

import uva.boringPoker.Card;

/**
 *
 * @author leavie
 */
public class Pile {

    private final int CAPACITY = 5;
    private final Card[] cards = new Card[CAPACITY]; // 5 spaces
    private final int TOP_INDEX = 0;
    private int size = 0; // 0...4

    public Pile() {

    }

    public Card getTop() {
        Card top = cards[TOP_INDEX];
        for (int i = 0; i < CAPACITY - 1; i++) { // 0..<4
            cards[i] = cards[i + 1]; // last turn: [3] <- [4]
        }
        --size;
        return top;
    }

    public void append(Card cd) {
        // precondition
        assert (size < CAPACITY && size > -1);
        assert (!cd.isFaceUp());

        cards[size++] = cd;
    }

    public void placeOnTop(Card cd) {
        assert (size < CAPACITY && size > -1);
        for (int i = 3; i >= 0; i--) {
            if (cards[i] == null) {
                cards[i] = cd;
                break;
            }
        }
        ++size;
    }

    public void print() {
        for (int i = 0; i < 4; i++) { // 4 cards for every pile
            System.out.printf("%-5s", cards[i]);
        }
    }

}
