/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.boringPoker;

/**
 *
 * @author leavie
 */
public class Pile { // 牌堆

    private static final int CAPACITY = 5; // 固定容量
    private final Card[] cards = new Card[CAPACITY]; // 5 spaces
    private int top = 0; // 指向牌堆的最上層
    private int tail = 0; // 指向牌堆的最下層
    private int size = 0; // 目前有多少卡牌

    public Card getTop() { // 先取再移動
        // percondintion
        assert (size > 0);
        
        Card topCard = cards[top];
        cards[top] = null;
        ++top; // 向後移動
        top %= CAPACITY; // 再給定容量中循環索引位置
        --size;
        return topCard;
    }

    public void append(Card cd) { // 先放置在移動，向後
        // precondition
        assert (size < CAPACITY);
        
        cards[tail] = cd;
        ++tail;
        tail %= CAPACITY;
        ++size;

    }

    public void placeOnTop(Card cd) { // 先移動再放置，向前
        top += (CAPACITY - 1);
        top %= CAPACITY;
        cards[top] = cd;
        ++size;
    }

    public void print() { // 從 top 開始向下列印四張
        for (int i = top, count = 0; count++ < 4; i = (i + 1) % CAPACITY) {
            System.out.printf("%-5s", cards[i]);
        }
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = top, count = 0; count++ < 4; i = (i + 1) % CAPACITY) {
            result += String.format("%5s", cards[i]);
        }
        return result;
    }

}
