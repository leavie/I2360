package uva.shared;

/**
 *
 * @author leavie
 */
public class Position {

    // fields
    private int xCoord; // x-coordinate
    private int yCoord; // y-coordinate
    private Direction faceOn; // 面向

    public enum Direction { // 字符枚舉
        N,
        E,
        S,
        W
    }

    // constructor
    public Position(int x, int y, Direction dir) {
        this.xCoord = x;
        this.yCoord = y;
        this.faceOn = dir;
    }

    // getter setter
    public int getxCoord() {
        return xCoord;
    }

    public void setxCoord(int x) {
        this.xCoord = x;
    }

    public int getyCoord() {
        return yCoord;
    }

    public void setyCoord(int y) {
        this.yCoord = y;
    }

    public Direction getFaceOn() {
        return faceOn;
    }

    public void setFaceOn(Direction dir) {
        faceOn = dir;
    }

    // non-static
    public void turnLeft() { // anti-clockwisely
        faceOn = getAntiClockwiseDirection(faceOn);
    }

    public void turnRight() { // clockwisely
        faceOn = getClockwiseDirection(faceOn);
    }


    public void forward() {
        switch (faceOn) {
            case N:
            ++yCoord;
            break;
            case S:
            --yCoord;
            break;
            case E:
            ++xCoord;
            break;
            case W:
            --xCoord;
            break;
        }
    }

    public void rollback() { // 倒退
        switch (faceOn) {
            case N:
            yCoord--;
            break;
            case S:
            yCoord++;
            break;
            case E:
            xCoord--;
            break;
            case W:
            xCoord++;
            break;
        }
    }

    public void print() {
        System.out.println(this);
    }

    // override
    @Override
    public String toString() {
        return "(x,y): (" + getxCoord() + "," + getyCoord() + ") direction: " + getFaceOn();
    }


    // static
    public static Direction getDirection(int dirNumber) {
        switch (dirNumber) {
            case 0:
            return Direction.N;
            case 1:
            return Direction.E;
            case 2:
            return Direction.S;
            case 3:
            return Direction.W;
        }
        throw new IllegalArgumentException("Unsupported direction");
    }

    public static Direction getClockwiseDirection(Direction direcion) {
        return getDirection((direcion.ordinal() + 1) % 4);
    }

    public static Direction getAntiClockwiseDirection(Direction direcion) {
        return getDirection((direcion.ordinal() + 3) % 4);
    }

    public static Direction getBouncedDirection(Direction direcion) {
        return getDirection((direcion.ordinal() + 2) % 4);
    }

    // test driver
    public static void main(String[] args) {
        Position pos1 = new Position(3, 5, Direction.N);
        System.out.println("Test pos1-----");
        pos1.print();
        pos1.turnLeft();
        System.out.println("tureLeft: " + pos1);
        pos1.forward();
        System.out.println("forward: " + pos1);
        pos1.turnRight();
        System.out.println("tureRight: " + pos1);
    }

}
