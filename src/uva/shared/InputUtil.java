/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uva.shared;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author leavie
 */
public class InputUtil {

    public static void main(String[] args) throws FileNotFoundException {
        InputUtil.getFile(args[0]);

    }

    public static File getFile(String path) {

        File file = new File(path);

        if (!file.exists()) {
            System.out.println("File not exists: " + file.getName());
        }
        return file;
    }

    public static Scanner inputFromPath(String path) {
        Scanner input;
        File file = InputUtil.getFile(path);
        // 判斷讀取形式
        try {
            input = new Scanner(file);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            System.out.println("Input below manually:");
            input = new Scanner(System.in);
        }
        return input;
    }

    public static void handleArgs(String[] args) {
        for (String arg : args) {
            System.out.println("arg: " + arg);
        }

        if (args.length == 0) {
            System.exit(1);
        }
    }

}
