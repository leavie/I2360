/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package help;

import java.util.Scanner;

class TreeNode {

    int value; //值
    TreeNode left_Node; //左節點
    TreeNode right_Node; //右節點

    public TreeNode(int value) //指到自己的指標
    {
        this.value = value;
        this.left_Node = null;
        this.right_Node = null;
    }
}

class BinarySearch {

    public TreeNode rootNode; //TreeNode是指標

    public void Add_Node_To_Tree(int value) //插入函式
    {
        if (rootNode == null) //樹根是NULL，沒有樹根
        {
            rootNode = new TreeNode(value); //最一開始建立新的節點，順便傳value
            //rootNode指到TreeNode
        }
        TreeNode currentNode = rootNode;  //一開始currentNode指到樹根
        while (true) {
            if (value < currentNode.value) // 一開始currentNode的value > 傳進來的value
            {
                if (currentNode.left_Node == null) {
                    currentNode.left_Node = new TreeNode(value);//  就擺左子樹(左邊)
                    return;
                } else {
                    currentNode = currentNode.left_Node; //currentNode就指到currentNode的left_Node
                }
            } else //if(value > currentNode.value)    補充:二元搜尋樹沒有等於的值
            {
                if (currentNode.right_Node == null) {
                    currentNode.right_Node = new TreeNode(value); //  就擺右子樹(右邊)
                    return;
                } else {
                    currentNode = currentNode.right_Node;  //currentNode就指到currentNode的right_Node
                }
            }
        }
    }

    public boolean findTree(TreeNode node, int value) //搜尋函式
    {
        if (node == null) {
            return false;
        } else if (node.value == value) {
            return true;
        } else if (value < node.value) {
            return findTree(node.left_Node, value); // 擺左邊
            // 遞迴呼叫
        } else // if( value > node.value )
        {
            return findTree(node.right_Node, value); // 擺右邊
            // 遞迴呼叫
        }
    }

}

public class BSTHelp {

    public static void main(String[] args) {
        // definition
        BinarySearch bst = new BinarySearch();
        int[] arr = {2, 3, 4, 8, 6, 2, 4};
        Scanner input = new Scanner(System.in);

        // add to bst
        for (int i = 0; i < arr.length; i++) {
            bst.Add_Node_To_Tree(arr[i]);
        }
        
        // print arr
        for (int i = 0; i < arr.length; i++) {
            System.out.print("|" + arr[i]);
        }
        System.out.println("");

        // prompt a number to search
       while(true) {
           System.out.println("input a number:"); 
           int number = input.nextInt();
           String result =  bst.findTree(bst.rootNode, number) ? "found" : "not found";
           System.out.println("find " + number + " in bst is: " + result);
        }

    }

}
