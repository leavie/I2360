package ch6.ex_06_33;

/**
 output: Current date and time is May 16, 2012 10:34:23
*/

 public class CurrentDateAndTime {
 	static final int GMT_TAIWAN = 8;

 	public static void main(String[] args) {
 		new CurrentDateAndTime().displayCurrentTime();
 	}

 	public static long milliToSec(long millis) {
 		return millis / 1000;
 	}

 	public static String getMonthName(int month) {
 		String monthName = "";
 		switch (month) {
 			case 1: monthName = "January"; break;
 			case 2: monthName = "February"; break;
 			case 3: monthName = "March"; break;
 			case 4: monthName = "April"; break;
 			case 5: monthName = "May"; break;
 			case 6: monthName = "June"; break;
 			case 7: monthName = "July"; break;
 			case 8: monthName = "August"; break;
 			case 9: monthName = "September"; break;
 			case 10: monthName = "October"; break;
 			case 11: monthName = "November"; break;
 			case 12: monthName = "December";
 		}
 		return monthName;
 	}
 	public static int getNumberOfDaysInMonth(int month, int year) {
		switch (month) {
			case 1:
			return 31;
			case 2:
			if (isLeap(year))
				return 29;
			else
				return 28;
			case 3:
			return 31;
			case 4:
			return 30;
			case 5:
			return 31;
			case 6:
			return 30;
			case 7:
			return 31;
			case 8:
			return 31;
			case 9:
			return 30;
			case 10:
			return 31;
			case 11:
			return 30;
			case 12:
			return 31;
		}
		return -1;
	}
 	public static boolean isLeap(int year) {
 		return year % 400 == 0 || year % 4 == 0 && year % 10 != 0;
 	}

	private final long currentTime = System.currentTimeMillis(); // since GMT+00 (Jan, 1, 1970 00:00:00))

	private long numberOfDaysRemain = getTotalNumberOfDays();

	public void displayCurrentTime() { // (month, day, year, hour, min, sec);
		long currentTimeInSec = milliToSec(currentTime);
		int sec = (int) currentTimeInSec % 60;
		int min = (int) currentTimeInSec / 60 % 60;
		int hour = (int) currentTimeInSec / 3600 % 24 + GMT_TAIWAN;
		int year = getCurrentYear();
		int month = getCurrentMonth(year);
		int day = getCurrentDay();
		System.out.println( getMonthName(month) + " " + day + ", " + year + " " + hour + ":" + min + ":" + sec);
	}
	public long getTotalNumberOfDays() {
		return milliToSec(currentTime) / 3600 / 24;
 	}
	public int getCurrentYear() {
		int year = 1970;
		int numberOfDaysInYear;
		do {
			numberOfDaysInYear = isLeap(year++) ? 366 : 365;
			numberOfDaysRemain -= numberOfDaysInYear;
		} while(numberOfDaysRemain >= numberOfDaysInYear);
		return year;
	}
	public int getCurrentMonth(int year) {
		int month = 1;
		int numberOfDaysInMonth;
		do {
			numberOfDaysInMonth = getNumberOfDaysInMonth(month++, year);
			numberOfDaysRemain -= numberOfDaysInMonth;
		} while(numberOfDaysRemain >= numberOfDaysInMonth);
		return month;
	}
	public int getCurrentDay() {
		return (int)(numberOfDaysRemain);
	}
}
