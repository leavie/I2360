/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch12;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 *
 * @author leavie
 */
public class WriteData {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("texts/scores.txt");

        if (file.exists()) {
            System.out.println("File alreay exists " + file.getName());
            System.exit(1);
        }

        // create a file
        PrintWriter output = new PrintWriter(file);

        // wirte formatted output to the file
        output.print("John T Smith ");
        output.println(90);
        output.print("Eric K Jones ");
        output.println(85);

        // close the file
        output.close();

    }

}
