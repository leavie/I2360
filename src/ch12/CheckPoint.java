package ch12;

/**
 *
 * @author leavie
 */
public class CheckPoint {

    static class ch12_05 {

        public static void main(String[] args) {
            try {
                int value = 30;
                if (value < 40) {
                    throw new Exception("value is too small");
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            System.out.println("Continue after the catch block");
        }
    }

    static class ch12_06 {

        public static void main(String[] args) {
            System.out.println("a");
            a();
            System.out.println("b");
            b();
        }

        public static void a() {
            for (int i = 0; i < 10; i++) {
                System.out.println(i + " ");
                try {
                    System.out.println(1 / 0);
                } catch (Exception ex) {
                }
            }
        }

        public static void b() {
            try {
                for (int i = 0; i < 2; i++) {
                    System.out.println(i + " ");
                    System.out.println(1 / 0);
                }
            } catch (Exception ex) {
            }
        }
    }

    static class ch12_08 {
        public static void main(String[] args) {
            Object o = new Object();
            String d = (String)o; // ClassCastException: java.lang.Object cannot be cast to java.lang.String
        }
    }

    static class ch12_14 {

        public static void main(String[] args) {
            try {
                int[] list = new int[10];
                System.out.println("list[10] is " + list[10]);
            } catch (ArithmeticException ex) {
                System.out.println("ArithmeticException");
            } catch (RuntimeException ex) {
                System.out.println("RuntimeException");
            } catch (Exception ex) {
                System.out.println("Exception");
            }
        }
    }

    static class ch12_15 {

        public static void main(String[] args) {
            try {
                method();
                System.out.println("After the method call");
            } catch (ArithmeticException ex) {
                System.out.println("ArithmeticException");
            } catch (RuntimeException ex) {
                System.out.println("RuntimeException");
            } catch (Exception e) {
                System.out.println("Exception");
            }
        }

        static void method() throws Exception {
            System.out.println(1 / 0);
        }
    }

    static class ch12_16 {

        public static void main(String[] args) {
            try {
                method();
                System.out.println("After the method call");
            } catch (RuntimeException ex) {
                System.out.println("RuntimeException in main");
            } catch (Exception ex) {
                System.out.println("Exception in main");
            }
        }

        static void method() throws Exception {
            try {
                String s = "abc";
                System.out.println(s.charAt(3));
            } catch (RuntimeException ex) {
                System.out.println("RuntimeException in method()");
            } catch (Exception ex) {
                System.out.println("Exception in method()");
            }
        }
    }

    static class ch12_20 {

        public void m(int value) throws Exception {
            if (value < 40) {
                throw new Exception("value is too small");
            }
        }
    }

    static class ch12_21 {

        public static void main(String[] args) {
            try {
                System.out.println("statement1");
                System.out.println("statement2");
                throwEx();
                System.out.println("statement3");
            } catch (Exception ex) {
            } finally {
                System.out.println("statement4");
            }
            System.out.println("statement5");
        }

        public static void throwEx() throws Exception {
            throw new Exception();
        }

    }

    static class ch12_22 {

        public static boolean isNumeric(String token) {
            try {
                Double.parseDouble(token);
                return true;
            } catch (java.lang.NumberFormatException ex) {
                return false;
            }
        }

        public static boolean isNumericRe(String token) {
            for (int i = 0; i < token.length(); i++) {
                if (!Character.isDigit(token.charAt(i)) && token.charAt(i) != '.') {
                    return false;
                }
            }
            return true;
        }

        public static void main(String[] args) {
            System.out.println(isNumericRe("123.g3"));
        }
    }

    static class ch12_23 {

        public static void main(String[] args) throws Exception {
            try {
                System.out.println("statement1");
                System.out.println("statement2");
                throwEx();
                System.out.println("statement3");
            } catch (RuntimeException ex) {
                System.out.println("catching ex");
                System.out.println(ex.getMessage());
                System.out.println(ex);
            } catch (Exception ex2) {
                System.out.println("rethrow ex2");
                throw ex2;
            } finally {
                System.out.println("statement4");
            }
            System.out.println("statement5");
        }

        public static void throwEx() throws Exception, RuntimeException {
//            throw new Exception();
            throw new RuntimeException("msg");
        }

    }
}
