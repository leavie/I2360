package ch12;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;

/**
 *
 * @author leavie
 */
public class TestFile {
    public static void main(String[] args) {
        File file = new File("images/I2B69.png");


        System.out.println("存在嗎？ " + file.exists());
        System.out.println("is directory? " + file.isDirectory());
        System.out.println("is File? " + file.isFile());
        System.out.println("Can it be read ? " + file.canRead());
        System.out.println("Can it be written? " + file.canWrite());
        System.out.println("The file has: " + file.length() + " bytes");
        System.out.println("file name" + file.getName());
        System.out.println("is Abs? " + file.isAbsolute());
        System.out.println("Abs path: " + file.getAbsolutePath());

        file.renameTo(new File("src/images/I2B69.png"));
        System.out.println("After rename, file name" + file.getName()); // 注意：執行改名的操作後，即使目標file被更名了， file object的property也不會更新

    }

}
