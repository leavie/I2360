/*
 * object: replace all occurance of old string in source file to new string in new file
 * command usage: java ReplaceText sourcefile targetfile oldstring newstring
 * 
 */
package ch12;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author leavie
 */
public class ReplaceText {
    public static void main(String[] args) throws FileNotFoundException {
        for (String arg : args) {
            System.out.println("arg: " + arg);
        }

        if (args.length != 4) {
            System.exit(1);
        }

        String sourcefile = args[0];
        String targetfile = args[1];
        String oldText = args[2];
        String newText = args[3];

        File source = new File(sourcefile);
        File target = new File(targetfile);

        if (!source.exists()) {
            System.out.println("source not found");
            System.exit(2);
        }

        try (Scanner input = new Scanner(source);
                PrintWriter output = new PrintWriter(target)) {
            while (input.hasNext()) {
                String line = input.nextLine();
                String newLine = line.replaceAll(oldText, newText);
                output.println(newLine);
            }
        }
    }
}
