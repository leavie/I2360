/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch12;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author leavie
 */
public class InputMismatchExceptionDemo {

    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        boolean continueInput = true;
        methodA();
        methodB();
        do {
            try {
                System.out.println("Enter an Integer");
                int number = input.nextInt();

                System.out.println("the number entered is: " + number);

                continueInput = false;

            } catch (InputMismatchException ex) {
                System.out.println("Try again. (Incorrect input: an integr input is required)");

                input.nextLine(); // discard this line

            }
        } while (continueInput);
    }

    static void methodA() throws InputMismatchException {
    }

    static void methodB() throws IOException {
    }

}
