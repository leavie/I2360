/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author leavie
 */
public class ReadData {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("texts/scores.txt");
        Scanner input = new Scanner(file);

        while (input.hasNext()) { // lopp until meet EOF
            String first = input.next();
            String mid = input.next();
            String last = input.next();
            int score = input.nextInt();

            System.out.println(
                    first + " " + mid + " " + last + " " + score);
        }

        input.close();
    }
}
