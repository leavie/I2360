/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch12;

/**
 *
 * @author leavie
 */
public class TestException_List_12_6 {

    public static void main(String[] args) {
        try {
            System.out.println(sum(new int[]{1, 2, 3, 4, 5, 10}));
        } catch (Exception ex) {
            ex.printStackTrace();

            System.out.println("\n" + ex.getMessage());
            System.out.println("\n" + ex.toString());

            System.out.println("\\nTrace Info Obtained from getStackTrace");
            StackTraceElement[] traceElements = ex.getStackTrace();
            for (int i = 0; i < traceElements.length; i++) {

            }
        }

    }

    public static int sum(int[] numbers) {
        int result = 0;
        for (int i = 0; i <= numbers.length; i++) {
            result += numbers[i];
        }

        return result;
    }

}
