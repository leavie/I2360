package ch12;

/**
 *
 * @author leavie
 */
public class CircleWithException {

    // data field
    private double radius = 1;

    /**
     * The number of objects is created
     */
    private static int numberOfObjects = 0;

    /**
     * Construct a circle with radius 1
     */
    // no-arg constructor
    public CircleWithException() {
        numberOfObjects++;
    }

    public CircleWithException(double newRadius) {
        radius = newRadius;
        numberOfObjects++;
    }

    public double getRadius() {
        return radius;
    }

    /**
     * Set a new radius for this circle
     */
    void setRadius(double newRadius) throws IllegalArgumentException {
//        radius = (newRadius >= 0) ? newRadius : 0;
        if (newRadius >= 0) {
            radius = newRadius;
        } else {
            throw new IllegalArgumentException("Radius cannot be negative");
        }
    }

    public static int getNumberOfObjects() {
        return numberOfObjects;
    }

    /**
     * Return the area of this circle
     */
    double getArea() {
        return radius * radius * Math.PI;
    }

}
