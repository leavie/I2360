package oopmidterm;

/**
 *
 * @author leavie
 */
public class TestBST {
    public static void main(String[] args) {
        BST bst = new BST();
        System.out.println("add 1 is: " + bst.add(1));
        System.out.println("add 3 is: " + bst.add(3));
        System.out.println("add 5 is: " + bst.add(5));
        System.out.println("add 3 is: " + bst.add(3));
        System.out.println("add 9 is: " + bst.add(9));
        System.out.println("add 5 is: " + bst.add(5));
        System.out.println("add 8 is: " + bst.add(8));
        System.out.println("add 10 is: " + bst.add(10));
        System.out.println("add 10 is: " + bst.add(10));
        System.out.println("search 1 is: " + bst.search(1));
        System.out.println("search 9 is: " + bst.search(9));
        System.out.println("search 2 is: " + bst.search(2));
        System.out.println("search 7 is: " + bst.search(7));
        System.out.println("search 3 is: " + bst.search(3));        
    }
}
