package oopmidterm;

/**
 *
 * @author leavie
 */
public class BST {

    private static class Node {

        private int key;
        private Node left;
        private Node right;

        public Node() {
            left = right = null;
        }

        public Node(int key) {
            this();
            this.key = key;
        }
    }

    private Node root;

    public BST() {
        root = null;
    }

    public int add(int key) {
        Node current = root;
        Node prev;
        Node toInsert = new Node(key);

        if (current == null) {
            root = toInsert;
            return 1;
        }

        while (current != null) {
            if (current.key == key) {
                return 0;
            }
            if (key > current.key) {
                prev = current;
                current = current.right;

                // insert at tail
                if (current == null) {
                    prev.right = toInsert;
                }
            } else {
                prev = current;
                current = current.left;

                // insert at tail
                if (current == null) {
                    prev.left = toInsert;
                }
            }
        }
        // insert at tail
//            if (key > prev.key) {
//                prev.right = toInsert;
//            } else {
//                prev.left = toInsert;
//            }
//        }
        return 1;

    }

    public int search(int key) {
        Node current = root;
        while (current != null) {
            if (current.key == key) {
                return 1;
            }
            if (key > current.key) {
                current = current.right;
            } else {
                current = current.left;
            }
        }
        return 0;
    }
}
