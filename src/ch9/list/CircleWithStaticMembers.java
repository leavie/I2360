/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch9.list;

/**
 *
 * @author leavie
 */
public class CircleWithStaticMembers {
    public static void main(String[] args) {
        // add your code here...
        
    }
    static double radius;
    static int numberOfObjects = 0;

    
    public CircleWithStaticMembers() {
        numberOfObjects++;
    }
    public static int getNumberOfObjects(double newRadius) {
        radius = newRadius;
        return numberOfObjects;
    }



    
    public static double getArea() {
        return radius * radius * Math.PI;
    }

}
