/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch9.list;

/**
 *
 * @author leavie
 */
public class TestPassObject {

    public static void main(String[] args) {
        // create a Circle object with radius 1
        CircleWithPrivateDataFields myCircle
                = new CircleWithPrivateDataFields(1);
        System.out.println("\n" + "Radius is " + myCircle.getRadius());
        // Print areas for radius 1,2,3,4 and 5
        int n = 5;
        printAreas(myCircle, n);
        // See my cirlce.radius and times
        System.out.println("n is " + n);

    }

    public static void printAreas(
            CircleWithPrivateDataFields c, int times) {

        System.out.println("Radius \t\tArea");
        while (times >= 1) {
            System.out.println(c.getRadius() + "\t\t" + c.getArea());
            c.setRadius(c.getRadius() + 1);
            times--;
        }
    }
}
