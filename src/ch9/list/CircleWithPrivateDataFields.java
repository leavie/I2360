package ch9.list;

/**
 *
 * @author leavie
 */
public class CircleWithPrivateDataFields {

    // data field
    private double radius = 1;

    /**
     * The number of objects is created
     */
    private static int numberOfObjects = 0;

    /**
     * Construct a circle with radius 1
     */
    // no-arg constructor
    public CircleWithPrivateDataFields() {
        numberOfObjects++;
    }

    public CircleWithPrivateDataFields(double newRadius) {
        radius = newRadius;
        numberOfObjects++;
    }

    public double getRadius() {
        return radius;
    }

    /**
     * Set a new radius for this circle
     */
    void setRadius(double newRadius) {
        radius = (newRadius >= 0) ? newRadius : 0;
    }

    public static int getNumberOfObjects() {
        return numberOfObjects;
    }

    /**
     * Return the area of this circle
     */
    double getArea() {
        return radius * radius * Math.PI;
    }

}
