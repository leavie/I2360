package ch9.ex;

import java.util.Scanner;

/**
 * Write a test program that prompts the user to enter values for a, b, and c
 * and displays the result based on the discriminant. If the discriminant is
 * positive, display the two roots. If the discriminant is 0, display the one
 * root. Otherwise, display “The equation has no roots.
 *
 * @author leavie
 */
public class Exercise_09_10 {

    public static void main(String[] args) {
        // add your code here...
        int a, b, c;
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        QuadraticEquation qe = new QuadraticEquation(a, b, c);
        if (qe.getDiscrimnant() > 0) {
            System.err.println("root1: " + qe.getRoot1() + " root2: " + qe.getRoot2());
        } else if (qe.getDiscrimnant() == 0) {
            System.out.println("root: " + qe.getRoot1());
        } else {
            System.out.println("no root");
        }
    }
}
