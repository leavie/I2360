package ch9.ex;

import java.util.Scanner;

/**
 * Write a test program that prompts the user to enter a, b, c, d, e, and f and
 * displays the result. If ad - bc is 0, report that “The equation has no
 * solution.
 *
 * @author leavie
 */
public class Exercise_09_11 {

    public static void main(String[] args) {
        // add your code here...
        double a, b, c, d, e, f;
        Scanner input = new Scanner(System.in);
        a = input.nextDouble();
        b = input.nextDouble();
        c = input.nextDouble();
        d = input.nextDouble();
        e = input.nextDouble();
        f = input.nextDouble();

        LinearEquation le = new LinearEquation(a, b, c, d, e, f);
        if (le.isSolvabe()) {
            System.out.println("result is: (" + le.getX() + ", " + le.getY() + ")");
        } else {
            System.out.println("The equation has no solution");
        }
    }
}
