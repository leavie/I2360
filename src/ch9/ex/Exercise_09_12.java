package ch9.ex;

import java.util.Scanner;

/**
 * Write a program that prompts user to input two distinct line segment if they
 * have intersection return the line segment if not ....
 *
 * @author leavie
 */
/** test data
 * @author leavie
 * --------------------------
 * 兩非平行線段無交點
 * Input x, y of end point A of line 1
 * 0 0
 * Input x, y of end point B of line 1
 * 5 5
 * Input x, y of end point C of line 2
 * 1 0
 * Input x, y of end point D of line 2
 * 2 -1
 * line1: start: (x: 0.0, y: 0.0), end: (x: 5.0, y: 5.0)
 * line2: start: (x: 1.0, y: 0.0), end: (x: 2.0, y: -1.0)
 * Result: Intersection is: null
 * 
 * -------------------------------------
 * 有交點
 * Input x, y of end point A of line 1
 * 0 0 
 * Input x, y of end point B of line 1
 * 5 5
 * Input x, y of end point C of line 2
 * 0 2
 * Input x, y of end point D of line 2
 * 2 -1
 * line1: start: (x: 0.0, y: 0.0), end: (x: 5.0, y: 5.0)
 * line2: start: (x: 0.0, y: 2.0), end: (x: 2.0, y: -1.0)
 * Result: Intersection is: start: (x: 0.8, y: 0.8), end: (x: 0.8, y: 0.8)
 * 
 * ---------------------------------------
 * 有交點
 * Input x, y of end point A of line 1
 * 0 0
 * Input x, y of end point B of line 1
 * 5 5
 * Input x, y of end point C of line 2
 * 5 0
 * Input x, y of end point D of line 2
 * 0 5
 * line1: start: (x: 0.0, y: 0.0), end: (x: 5.0, y: 5.0)
 * line2: start: (x: 5.0, y: 0.0), end: (x: 0.0, y: 5.0)
 * Result: Intersection is: start: (x: 2.5, y: 2.5), end: (x: 2.5, y: 2.5)
 * 
 * ----------------------------------
 * 水瓶與鉛直線 有一交點
 * Input x, y of end point A of line 1
 * 3 1
 * Input x, y of end point B of line 1
 * 10 1
 * Input x, y of end point C of line 2
 * 5 10
 * Input x, y of end point D of line 2
 * 5 0
 * line1: start: (x: 3.0, y: 1.0), end: (x: 10.0, y: 1.0)
 * line2: start: (x: 5.0, y: 10.0), end: (x: 5.0, y: 0.0)
 * Result: Intersection is: start: (x: 5.0, y: 1.0), end: (x: 5.0, y: 1.0)
 */
public class Exercise_09_12 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input x, y of end point A of line 1");
        double l1x1 = scan.nextDouble();
        double l1y1 = scan.nextDouble();
        System.out.println("Input x, y of end point B of line 1");
        double l1x2 = scan.nextDouble();
        double l1y2 = scan.nextDouble();

        System.out.println("Input x, y of end point C of line 2");
        double l2x1 = scan.nextDouble();
        double l2y1 = scan.nextDouble();
        System.out.println("Input x, y of end point D of line 2");
        double l2x2 = scan.nextDouble();
        double l2y2 = scan.nextDouble();

        Line line1 = new Line(l1x1, l1y1, l1x2, l1y2);
        Line line2 = new Line(l2x1, l2y1, l2x2, l2y2);
        Line intersection = Line.intersect(line1, line2);

        System.out.println("line1: " + line1);
        System.out.println("line2: " + line2);
        System.out.print("Result: ");
        System.out.println("Intersection is: " + intersection);

    }
}
