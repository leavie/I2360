package ch9.ex;

/**
 *
 * @author leavie
 */
public class Fan {

    enum SPEED_STATE {
        SLOW,
        MEDIUM,
        FAST,
    }

    private SPEED_STATE speed = SPEED_STATE.SLOW;
    private boolean on = false;
    private double radius = 5;
    private String color = "blue";

    Fan() {
        this.speed = SPEED_STATE.SLOW;
        this.on = false;
        this.radius = 5;
        this.color = "blue";
    }

    public SPEED_STATE getSpeed() {
        return speed;
    }

    public void setSpeed(SPEED_STATE speed) {
        this.speed = speed;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {

        String basic;
        basic = ", color: " + this.getColor() + ", radius: " + this.getRadius();
        String result;
        if (this.isOn()) {
            result = "speed: " + this.getSpeed() + basic;
        } else {
            result = basic + ", fan is off";
        }
        return result;
    }

}
