package ch9.ex;

import java.util.Scanner;

/**
 * Write a test program that prompts the user to enter a two-dimensional array
 * and displays the location of the largest element in the array
 *
 * @author leavie
 */
public class Exercise_09_13 {

    public static void main(String[] args) {
        // add your code here...
        System.out.println("Enter the number of rows and columns in the array:");
        System.out.println("Enter the array:");

        Scanner input = new Scanner(System.in);
        int row, column;
        row = input.nextInt();
        column = input.nextInt();
        double[][] a = new double[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                a[i][j] = Math.random() * 100;
            }
        }

        Location locationOflargestElement = Location.locateLargest(a);
        System.out.println("The location of the largest element is "
                + locationOflargestElement);
    }
}
