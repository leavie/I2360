/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch9.ex;

/**
 *
 * @author leavie
 */
public class Location {
    private int row;
    private int column;
    private double maxValue;

    public Location(int row, int column, double maxValue) {
        this.row = row;
        this.column = column;
        this.maxValue = maxValue;
    }
    
    
    public static Location locateLargest(double[][] a) {
        double maxValue;
        int maxRow;
        int maxColumn;
        
        maxRow = 0;
        maxColumn = 0;
        maxValue = a[0][0];
        for(int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] > maxValue) {
                    maxValue = a[i][j];
                    maxRow = i;
                    maxColumn = j;
                }
            }
        }
        return new Location(maxRow, maxColumn, maxValue);
    }

    @Override
    public String toString() {
        return maxValue + " at (" + row + ", " +  column +")";
    }
    
    
}
