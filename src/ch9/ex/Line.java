package ch9.ex;

/**
 *
 * @author leavie
 */
public class Line {

    private Point endPoint1;
    private Point endPoint2;
    private static int NUMBER_OF_TERMS = 3;

    public Line(double x1, double y1, double x2, double y2) {
        endPoint1 = new Point(x1, y1);
        endPoint2 = new Point(x2, y2);
    }
    public Line(Point p1, Point p2) {
        endPoint1 = p1;
        endPoint2 = p2;
    }

    // TODO
    public static Line intersect(Line line1, Line line2) { // intersect with two line segment
        LinearEquation le;
        double[] coefs1 = standardize(line1);
        double[] coefs2 = standardize(line2);
        le = new LinearEquation(coefs1, coefs2);
        if (isSegmentIntersect(coefs1, coefs2, line1, line2)) {
            if(le.isSolvabe()){
                return new Line(le.getX(), le.getY(), le.getX(), le.getY()); // 有一個交點
            } else {
                // TODO: 重疊的情況
                // 回傳被包含的兩個端點
            }
        }
        return null;
    }

    public static boolean isSegmentIntersect(double[] coefs1, double[] coefs2, Line line1, Line line2) {
        return line1.isCross(coefs2) && line2.isCross(coefs1);
    }

    boolean isCross(double[] a) { // Is this with two end points cross on line "a" above two sides
        // 以 a 直線為基準，判斷線段是否跨越 a
        double[][] sameSideMaxtrixOnB = maxtrixMultiply(new double[][] {{a[0]},{a[1]},{a[2]}}, EndPointToMatrix(this));
        return getCrossValue(sameSideMaxtrixOnB) <= 0;
    }

    private static double getCrossValue(double[][] sameSideMaxtrix) {
        double value = 1;
        for (double[] col : sameSideMaxtrix) {
            value *= col[0];
        }
        return sameSideMaxtrix[0][0] * sameSideMaxtrix[1][0];

        /*
            sameSideMatrix:

            [
                [j],
                [k]
            ]

            value = j * k
        */
    }


    private static double[] standardize(Line line) { // 回傳線段多項式 ax + by = e 的係數
        double x1, x2, y1, y2;
        x1 = line.endPoint1.getX();
        y1 = line.endPoint1.getY();
        x2 = line.endPoint2.getX();
        y2 = line.endPoint2.getY();

        double a, b, e;

        if (x1 - x2 == 0) {
            // throw new NullPointerException("分母不能為零");
            // TODO: check
            b = 0;
            a = 1;
            e = x1;
        } else {
            double m;
            m = (y1 - y2) / (x1 - x2);
            a = -m;
            b = 1;
            e = a * x1 + y1;
        }
        return new double[]{a, b, e};
    }

    private static double[][] EndPointToMatrix(Line line) {
        double x1, x2, y1, y2;
        x1 = line.endPoint1.getX();
        y1 = line.endPoint1.getY();
        x2 = line.endPoint2.getX();
        y2 = line.endPoint2.getY();
        return new double[][]{{x1, y1, -1}, {x2, y2, -1}};

        /*
            [
                [x1, y1, 1],
                [x2, y2, 1]
            ]
        */
    }

    private static double[][] maxtrixMultiply(double[][] a, double[][] b) {
        double[][] c = new double[b.length][a[0].length];
        int termSum;
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                termSum = 0;
                for (int k = 0; k < NUMBER_OF_TERMS; k++) {
                    termSum += a[k][j] * b[i][k];
                }
                c[i][j] = termSum;
            }

        }
        return c;

        /*
            [
                [a],
                [b],
                [e]
            ]

            MUL

            [
                [x1, y1, 1],
                [x2, y2, 1]
            ]

            EQUAL

            [
                [j],
                [k]
            ]
        */
    }

    @Override
    public String toString() {
        return "start: " + endPoint1
                + ", end: " + endPoint2;
    }

}
