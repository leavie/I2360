package ch9.ex;

/**
 * Write a test program that measures the execution time of sorting 100,000
 * numbers using selection sort.
 *
 * @author leavie
 */
public class Exercise_09_06 {

    private static int[] numbers;

    public static void main(String[] args) {
        StopWatch timer1 = new StopWatch();
        timer1.start();
        generateRandomNumbers(100000);
        selectionSort();
        timer1.stop();
        System.out.println("time elapsed: " + timer1.getElapsedTime());

    }

    private static void generateRandomNumbers(int quantity) {
        numbers = new int[quantity];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (int) (Math.random() * 10000); // whole number 0..<10000
        }

    }

    private static void selectionSort() {
        int maximumIndex;
        for (int i = 0; i < numbers.length; i++) {
            maximumIndex = i;
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] > numbers[maximumIndex]) {
                    maximumIndex = j;
                }
            }
            swapNumbers(i, maximumIndex);
        }
    }

    private static void swapNumbers(int leftIndex, int rightIndex) {
        int t;
        t = numbers[leftIndex];
        numbers[leftIndex] = numbers[rightIndex];
        numbers[rightIndex] = t;
    }
}
