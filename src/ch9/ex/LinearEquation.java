/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch9.ex;

/**
 *
 * @author leavie
 */
public class LinearEquation {

    private double a;
    private double b;
    private double c;
    private double d;
    private double e;
    private double f;

    public LinearEquation(double a, double b, double c, double d, double e, double f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public LinearEquation(double[] coef1, double[] coef2) {
        this.a = coef1[0];
        this.b = coef1[1];
        this.e = coef1[2];
        this.c = coef2[0];
        this.d = coef2[1];
        this.f = coef2[2];
    }

    public boolean isSolvabe() {
        return a * d - b * c != 0;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getD() {
        return d;
    }

    public double getE() {
        return e;
    }

    public double getF() {
        return f;
    }

    public double getX() {
        if (isSolvabe()) {
            return (e * d - b * f) / (a * d - b * c);
        }
        return 0;
    }

    public double getY() {
        if (isSolvabe()) {
            return (a * f - e * c) / (a * d - b * c);
        }
        return 0;
    }

}
