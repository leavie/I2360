/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch9.ex;

/**
 *
 * @author leavie
 */
public class RegularPolygon {
    int n;
    double side;
    double x;
    double y;

    public RegularPolygon() {
        this(3, 1, 0, 0);
    }
    
    RegularPolygon(int n, double side) {
        this(n, side, 0, 0);
    }

    public RegularPolygon(int n, double side, double x, double y) {
        this.n = n;
        this.side = side;
        this.x = x;
        this.y = y;
    }
    
    public double getPerimeter() {
        return n * side;
    }
    
    public double getArea() {
        return n * side * side / 4 / Math.tan(Math.PI / n);
    }
    
    
    
}
