/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch9.ex;

/**
 *
 * @author leavie
 */
public class QuadraticEquation {
    private int a;
    private int b;
    private int c;

    public QuadraticEquation(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }
    
    public int getDiscrimnant() {
        return b * b - 4 * a * c;
    }
    
    public double getRoot1() {
        int dis = this.getDiscrimnant();
        if (dis < 0)
            return 0;
        return (-b + Math.sqrt(this.getDiscrimnant())) / 2 / a;
    }
    
    public double  getRoot2() {
        int dis = this.getDiscrimnant();
        if (dis < 0)
            return 0;
        return (-b - Math.sqrt(this.getDiscrimnant())) / 2 / a;
    }
    
    
    
}
