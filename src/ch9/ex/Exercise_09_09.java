package ch9.ex;

/**
 * Write a test program that creates three RegularPolygon objects, created using
 * the no-arg constructor,usingRegularPolygon(6, 4) and using RegularPolygon(10,
 * 4, 5.6, 7.8). For each object, display its perimeter and area
 *
 * @author leavie
 */
public class Exercise_09_09 {

    public static void main(String[] args) {
        // add your code here...
        RegularPolygon rp1 = new RegularPolygon();
        RegularPolygon rp2 = new RegularPolygon(6, 4);
        RegularPolygon rp3 = new RegularPolygon(10, 4, 5.6, 7.8);

        RegularPolygon[] rpArr = new RegularPolygon[3];
        rpArr[0] = rp1;
        rpArr[1] = rp2;
        rpArr[2] = rp3;

        for (int i = 0; i < rpArr.length; i++) {
            System.out.println("regular polygon " + (i + 1) + ":");
            System.out.println("\tperimeter: " + rpArr[i].getPerimeter());
            System.out.println("\tarea: " + rpArr[i].getArea());
        }
    }
}
