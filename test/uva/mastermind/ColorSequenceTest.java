package uva.mastermind;

import org.junit.Test;

/**
 * Created by leavie on 2018/3/24.
 */
public class ColorSequenceTest {
    @Test
    public void random() throws Exception {
        System.out.println(ColorSequence.random());
    }

    @Test
    public void setColors() throws Exception {
        System.out.println(new ColorSequence("hello..."));
    }

    @Test
    public void comcom() {
        SolutionSpace space = new SolutionSpace();
        // computer is riddle also guess 電腦是出題者同事也是解題者
        ColorSequence riddle = ColorSequence.random();
        ColorSequence guess; // 猜測序列

        // 洩漏謎底
        System.out.println(riddle);
        while (!space.isBingo()) {
            // 從空間取得（下）一個猜測
            guess = space.next();

            System.out.println("-----------------");
            System.out.println("Turn: " + (space.getTimes()));
            System.out.println("Space left: " + space.size());
            System.out.println("My Guess is: " + guess);
            // 提示使用者比對謎底並數一數黑白針的數量，做為過濾標竿
            ColorSequence.compare(riddle, guess); // 比對結果儲存在出題者
            System.out.println("Compare my guess to your riddle.\nCount on pins for both black and white as clue and input below⬇ ");
            System.out.println(riddle.getPinsOfBlack() + ", " + riddle.getPinsOfWhite());
            space.setTargets(riddle.getPinsOfBlack(), riddle.getPinsOfWhite());
            System.out.println("-----------------");
        }
        System.out.println("結束");
    }


}