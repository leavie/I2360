/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ch9.ex.Line;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leavie
 */
public class TestLine {

    Line a = new Line(0, 0, 5, 5);
    Line b = new Line(0, 5, 5, 0);
    Line b1 = new Line(0, 0, 1, 1);

    public TestLine() {
    }

    @Test
    public void para() {
        Line intersect;
        intersect = Line.intersect(new Line(0, 5, 10, 5), new Line(2, 10, 8, 10));
        assertEquals(null, intersect);
    }

    @Test
    public void point() {
        assertEquals(new Line(2.5, 2.5, 2.5, 2.5).toString(), Line.intersect(a, b).toString());
    }
    
    @Test
    public void noPoint() {
        assertEquals(null, Line.intersect(b, b1));
    }

    @Test
    public void communicate() {
        assertEquals(Line.intersect(a, b).toString(), Line.intersect(b, a).toString());
        assertEquals(Line.intersect(b1, b).toString(), Line.intersect(b, b1).toString());
    }

}
